using System;
using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 1 argument.
    /// </summary>
    public partial class OneArgumentEventChannel<T> : ScriptableObject
    {
        public UnityAction<T> OnEventRaised;

        public void RaiseEvent(T t)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t);
            else
                Debug.LogWarning($"<color=yellow>[Events]</color> Noone listens to {name}");
        }
    }
}
