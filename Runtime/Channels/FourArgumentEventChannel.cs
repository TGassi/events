using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 4 argument.
    /// </summary>
    public class FourArgumentEventChannel<T, U, V, W> : ScriptableObject
    {
        public UnityAction<T, U, V, W> OnEventRaised;

        public void RaiseEvent(T t, U u, V v, W w)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t, u, v, w);
            else
                Debug.LogWarning($"<color=yellow>[Events]</color> Noone listens to {name}");
        }
    }
}