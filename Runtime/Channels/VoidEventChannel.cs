using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events without arguments.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/0 - Void EventChannel", order = 0)]
    public class VoidEventChannel : ScriptableObject
    {
        public UnityAction OnEventRaised;

        public void RaiseEvent()
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke();
            else
                Debug.LogWarning($"<color=yellow>[Events]</color> Noone listens to {name}");
        }
    }
}