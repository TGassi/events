using System;
using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 3 argument.
    /// </summary>
    public class ThreeArgumentEventChannel<T, U, V> : ScriptableObject
    {

        public UnityAction<T, U, V> OnEventRaised;

        public void RaiseEvent(T t, U u, V v)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t, u, v);
            else
                Debug.LogWarning($"<color=yellow>[Events]</color> Noone listens to {name}");
        }
    }
}