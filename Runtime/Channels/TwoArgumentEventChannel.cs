using System;
using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 2 argument.
    /// </summary>
    public class TwoArgumentEventChannel<T, U> : ScriptableObject
    {
        public UnityAction<T, U> OnEventRaised;

        public void RaiseEvent(T t, U u)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t, u);
            else
                Debug.LogWarning($"<color=yellow>[Events]</color> Noone listens to {name}");
        }
    }
}