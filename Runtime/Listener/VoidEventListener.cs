using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 0 arguments.
    /// </summary>
    public class VoidEventListener : EventListener
    {
        [SerializeField]
        private VoidEventChannel _channel;
        public VoidEventChannel Channel { get { return _channel; } }

        [SerializeField]
        private UnityEvent _onEventRaised;
        public UnityEvent OnEventRaised { get { return _onEventRaised; } }

        protected override void AddListener()
        {
            if (_channel)
                _channel.OnEventRaised += RaiseEvent;
        }
        protected override void RemoveListener()
        {
            if (_channel)
                _channel.OnEventRaised -= RaiseEvent;
        }

        void RaiseEvent()
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke();
        }
    }
}