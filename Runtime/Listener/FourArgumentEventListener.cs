using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Listen to Event with 4 arguments.
    /// </summary>
    public class FourArgumentEventListener<S, T, U, V, W> : EventListener where S : FourArgumentEventChannel<T, U, V, W>
    {
        [SerializeField]
        private FourArgumentEventChannel<T, U, V, W> _channel;
        public FourArgumentEventChannel<T, U, V, W> Channel { get { return _channel; } }

        [SerializeField]
        private UnityEvent<T, U, V, W> _onEventRaised;
        public UnityEvent<T, U, V, W> OnEventRaised { get { return _onEventRaised; } }

        protected override void AddListener()
        {
            if (_channel)
                _channel.OnEventRaised += RaiseEvent;
        }
        protected override void RemoveListener()
        {
            if (_channel)
                _channel.OnEventRaised -= RaiseEvent;
        }

        void RaiseEvent(T t, U u, V v, W w)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t, u, v, w);
        }
    }
}