using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    public abstract class EventListener : MonoBehaviour
    {
        void OnEnable()
        {
            AddListener();
        }
        protected abstract void AddListener();

        void OnDisable()
        {
            RemoveListener();
        }
        protected abstract void RemoveListener();
    }
}