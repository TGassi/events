using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 2 arguments.
    /// </summary>
    public class TwoArgumentEventListener<S, T, U> : EventListener where S : TwoArgumentEventChannel<T, U>
    {
        [SerializeField]
        private S _channel;
        public S Channel { get { return _channel; } }

        [SerializeField]
        private UnityEvent<T, U> _onEventRaised;
        public UnityEvent<T, U> OnEventRaised { get { return _onEventRaised; } }


        protected override void AddListener()
        {
            if (_channel)
                _channel.OnEventRaised += RaiseEvent;
        }
        protected override void RemoveListener()
        {
            if (_channel)
                _channel.OnEventRaised -= RaiseEvent;
        }

        protected void RaiseEvent(T t, U u)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t, u);
        }
    }
}