using TGassi.Events;
using UnityEngine;
using UnityEngine.Events;
using System.Diagnostics;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 1 argument.
    /// </summary>
    public class OneArgumentEventListener<S, T> : EventListener where S : OneArgumentEventChannel<T>
    {
        [SerializeField]
        private S _channel;
        public S Channel { get { return _channel; } }

        [SerializeField]
        private UnityEvent<T> _onEventRaised;
        public UnityEvent<T> OnEventRaised { get { return _onEventRaised; } }

        protected override void AddListener()
        {
            if (_channel)
                _channel.OnEventRaised += RaiseEvent;
        }
        protected override void RemoveListener()
        {
            if (_channel)
                _channel.OnEventRaised -= RaiseEvent;
        }

        void RaiseEvent(T t)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t);
        }
    }
}