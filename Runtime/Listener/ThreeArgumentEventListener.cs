using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    /// <summary>
    /// Raise Events with 3 arguments.
    /// </summary>
    public class ThreeArgumentEventListener<S, T, U, V> : EventListener where S : ThreeArgumentEventChannel<T, U, V>
    {
        [SerializeField]
        private S _channel;
        public S Channel { get { return _channel; } }

        [SerializeField]
        private UnityEvent<T, U, V> _onEventRaised;
        public UnityEvent<T, U, V> OnEventRaised { get { return _onEventRaised; } }


        protected override void AddListener()
        {
            if (_channel)
                _channel.OnEventRaised += RaiseEvent;
        }
        protected override void RemoveListener()
        {
            if (_channel)
                _channel.OnEventRaised -= RaiseEvent;
        }

        void RaiseEvent(T t, U u, V v)
        {
            if (OnEventRaised != null)
                OnEventRaised.Invoke(t, u, v);
        }
    }
}