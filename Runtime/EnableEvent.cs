using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    public class EnableEvent : MonoBehaviour
    {
        [SerializeField]
        UnityEvent _onEnabled = new UnityEvent();

        void OnEnable()
        {
            _onEnabled.Invoke();
        }
    }
}
