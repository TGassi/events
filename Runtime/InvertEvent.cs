using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TGassi.Events
{
    public class InvertEvent : MonoBehaviour
    {
        [SerializeField]
        UnityEvent<bool> _invertedEvent = new UnityEvent<bool>();

        public void Invert(bool value)
        {
            _invertedEvent.Invoke(!value);
        }
    }
}
